"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.app = undefined;

require("babel-core/register");

require("babel-polyfill");

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

var _bodyParser = require("body-parser");

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _morgan = require("morgan");

var _morgan2 = _interopRequireDefault(_morgan);

var _socket = require("socket.io");

var _socket2 = _interopRequireDefault(_socket);

var _cors = require("cors");

var _cors2 = _interopRequireDefault(_cors);

var _http = require("http");

var _http2 = _interopRequireDefault(_http);

var _index = require("./api/v1/routes/index");

var _index2 = _interopRequireDefault(_index);

var _events = require("./api/v1/events");

var _events2 = _interopRequireDefault(_events);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

require('dotenv').config();

// routes


// events


var app = (0, _express2.default)();
var server = _http2.default.createServer(app);
var socket = (0, _socket2.default)(server);

// changing the mongoose promise to global promise
_mongoose2.default.Promise = global.Promise;

// database connection
_mongoose2.default.connect(process.env.db_url, { useNewUrlParser: true }).then(function () {
    console.clear();
    console.log("Database connection has been established");
}).catch(function (err) {
    console.log(err);
});

// app setup
app.use((0, _cors2.default)());
app.use((0, _morgan2.default)('dev'));

app.use(_bodyParser2.default.urlencoded({ extended: false }));
app.use(_bodyParser2.default.json());

app.use('/api/v1', _index2.default);

// error handling
app.use(function (req, res, next) {
    var error = new Error('Nothing to show here :)');
    error.status = 404;
    next(error);
});

app.use(function (error, req, res, next) {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

// make socket global
app.socket = socket;
new _events2.default(socket);

server.listen(process.env.PORT || 3000, function () {
    console.log('Listening on port 3000');
});

exports.app = app;