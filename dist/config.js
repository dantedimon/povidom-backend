'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var serverCfg = {
    port: 3000
};

var databaseCfg = {
    url: 'mongodb://localhost:27017/povidom2'
};

var jwtCfg = {
    expireIn: '1m',
    secret: 'zalupo'
};

exports.serverCfg = serverCfg;
exports.databaseCfg = databaseCfg;
exports.jwtCfg = jwtCfg;