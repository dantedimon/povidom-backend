'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _UserModel = require('../models/UserModel');

var _UserModel2 = _interopRequireDefault(_UserModel);

var _config = require('../../../config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AuthCheck = function () {
    function AuthCheck() {
        _classCallCheck(this, AuthCheck);
    }

    _createClass(AuthCheck, null, [{
        key: 'request',
        value: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res, next) {
                var token, user;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.prev = 0;
                                token = req.headers.authorization.split(" ")[1];

                                // console.log('token: ' + token);

                                if (!token) {
                                    _context.next = 15;
                                    break;
                                }

                                user = _jsonwebtoken2.default.verify(token, _config.jwtCfg.secret);
                                _context.next = 6;
                                return _UserModel2.default.findOne({
                                    _id: user._id
                                });

                            case 6:
                                user = _context.sent;

                                if (!user) {
                                    _context.next = 12;
                                    break;
                                }

                                req.user = user;
                                next();
                                _context.next = 13;
                                break;

                            case 12:
                                return _context.abrupt('return', res.status(401).json({
                                    message: 'You have no permission to access this data'
                                }));

                            case 13:
                                _context.next = 16;
                                break;

                            case 15:
                                return _context.abrupt('return', res.status(401).json({
                                    message: 'You have no permission to access this data'
                                }));

                            case 16:
                                _context.next = 26;
                                break;

                            case 18:
                                _context.prev = 18;
                                _context.t0 = _context['catch'](0);

                                if (!(_context.t0.name === 'TokenExpiredError')) {
                                    _context.next = 24;
                                    break;
                                }

                                return _context.abrupt('return', res.status(401).json({
                                    message: 'Your access token has been expired. Please login again'
                                }));

                            case 24:
                                if (!(_context.t0.name === 'JsonWebTokenError')) {
                                    _context.next = 26;
                                    break;
                                }

                                return _context.abrupt('return', res.status(401).json({
                                    message: 'You have no permission to access this data'
                                }));

                            case 26:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this, [[0, 18]]);
            }));

            function request(_x, _x2, _x3) {
                return _ref.apply(this, arguments);
            }

            return request;
        }()
    }, {
        key: 'socket',
        value: function socket(_socket, next) {
            try {
                var token = _socket.handshake.query.token;

                if (token) {
                    _socket.user = _jsonwebtoken2.default.verify(token, _config.jwtCfg.secret);
                    next();
                } else {
                    next(new Error('Authentication error'));
                }
            } catch (error) {
                next(new Error('Authentication error'));
            }
        }
    }]);

    return AuthCheck;
}();

exports.default = AuthCheck;