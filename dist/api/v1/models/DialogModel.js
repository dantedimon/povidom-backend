'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.DialogEvents = exports.DialogModel = undefined;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _mongooseTrigger = require('mongoose-trigger');

var _mongooseTrigger2 = _interopRequireDefault(_mongooseTrigger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = new _mongoose2.default.Schema({
    user: {
        type: _mongoose2.default.Schema.Types.ObjectId,
        ref: 'user'
    },
    companion: {
        type: _mongoose2.default.Schema.Types.ObjectId,
        ref: 'user'
    },
    chat: {
        type: _mongoose2.default.Schema.Types.ObjectId,
        ref: 'chat'
    },
    messages: [{
        type: _mongoose2.default.Schema.Types.ObjectId,
        ref: 'message'
    }],
    notifications: {
        type: Boolean,
        default: true
    },
    active: {
        type: Boolean,
        default: false
    },
    unreadCount: {
        type: Number,
        default: 0
    }
}, {
    timestamps: true
});

var DialogEvents = (0, _mongooseTrigger2.default)(Schema, {
    events: {
        create: {
            populate: [{
                path: 'chat',
                select: '-__v',
                populate: {
                    path: 'members',
                    select: '-__v -password -dialogs'
                }
            }, {
                path: 'messages',
                select: '-__v'
            }]
        },
        update: true,
        remove: true
    },
    partials: [{
        eventName: 'active',
        triggers: 'active',
        populate: [{
            path: 'chat',
            select: '-__v',
            populate: {
                path: 'members',
                select: '-__v -password -dialogs'
            }
        }, {
            path: 'messages',
            select: '-__v'
        }]
    }],
    debug: false
});

/**
 *
 * @Class DialogModel
 */

var DialogModel = _mongoose2.default.model('dialog', Schema);

exports.DialogModel = DialogModel;
exports.DialogEvents = DialogEvents;