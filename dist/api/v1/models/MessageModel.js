'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.MessageEvents = exports.MessageModel = undefined;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _mongooseTrigger = require('mongoose-trigger');

var _mongooseTrigger2 = _interopRequireDefault(_mongooseTrigger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = new _mongoose2.default.Schema({
    chat: {
        type: _mongoose2.default.Schema.Types.ObjectId,
        ref: 'chat',
        required: true
    },
    content: {
        type: String,
        required: true
    },
    author: {
        type: _mongoose2.default.Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    read: {
        type: Boolean,
        default: false
    },
    edited: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});

var MessageEvents = (0, _mongooseTrigger2.default)(Schema, {
    events: {
        create: true,
        update: true,
        remove: {
            select: 'chat'
        }
    },
    partials: [{
        eventName: 'active',
        triggers: 'active',
        populate: [{
            path: 'chat',
            select: '-__v',
            populate: {
                path: 'members',
                select: '-__v -password -dialogs'
            }
        }, {
            path: 'messages',
            select: '-__v'
        }]
    }],
    debug: false
});

/**
 *
 * @Class MessageModel
 */

var MessageModel = _mongoose2.default.model('message', Schema);

exports.MessageModel = MessageModel;
exports.MessageEvents = MessageEvents;