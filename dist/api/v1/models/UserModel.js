'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _config = require('../../../config');

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = new _mongoose2.default.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    online: {
        type: Boolean,
        default: false
    },
    lastActivity: {
        type: Number,
        default: Date.now()
    },
    avatar: {
        type: String,
        default: ''
    },
    palette: {
        type: Number,
        default: Math.floor(Math.random() * (17 + 1))
    },
    dialogs: {
        type: [{
            type: _mongoose2.default.Schema.Types.ObjectId,
            ref: 'dialog'
        }],
        default: []
    }
});

// methods
Schema.methods.hashPassword = function (password) {
    var _this = this;

    var saltRounds = 10;

    return _bcrypt2.default.hash(password, saltRounds).then(function (hash) {
        if (hash) {
            _this.password = hash;
        }
    });
};

Schema.methods.validPassword = function (password) {
    return _bcrypt2.default.compare(password, this.password);
};

Schema.methods.generateJWT = function () {
    var payload = void 0,
        cert = void 0,
        setup = void 0;

    payload = {
        _id: this._id
    };

    cert = _config.jwtCfg.secret;

    setup = {
        expiresIn: _config.jwtCfg.expireIn
    };

    return _jsonwebtoken2.default.sign(payload, cert);
};

// statics
Schema.statics.updateStatus = function (userID, _ref) {
    var online = _ref.online,
        lastActivity = _ref.lastActivity;

    return this.findByIdAndUpdate(userID, {
        $set: { lastActivity: lastActivity, online: online }
    });
};

Schema.statics.usernameExist = function (username) {
    return this.findOne({
        username: username
    });
};

Schema.statics.getSelf = function (userID) {
    return this.findOne({
        _id: userID
    }).toSelf();
};

// queries
Schema.query.toSelf = function () {
    return this.select('-password -chats -__v');
};

Schema.query.toContactPrev = function () {
    return this.select('-password -__v');
};

/**
 *
 * @class User
 */
var User = _mongoose2.default.model('user', Schema);

exports.default = User;