'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Schema = new _mongoose2.default.Schema({
    type: String,
    title: {
        type: String,
        default: ''
    },
    avatar: {
        type: String,
        default: ''
    },
    active: {
        type: Boolean,
        default: false
    },
    members: [{
        type: _mongoose2.default.Schema.Types.ObjectId,
        ref: 'user'
    }]
}, {
    timestamps: true
});

/**
 *
 * @Class ChatModel
 */
var ChatModel = _mongoose2.default.model('chat', Schema);

exports.default = ChatModel;