'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _EventEmitter = require('../events/EventEmitter');

var _EventEmitter2 = _interopRequireDefault(_EventEmitter);

var _MessageModel = require('../models/MessageModel');

var _DialogModel = require('../models/DialogModel');

var _AuthCheck = require('../middleware/AuthCheck');

var _AuthCheck2 = _interopRequireDefault(_AuthCheck);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var router = _express2.default.Router();

// models


// middleware

// controller
var MessageController = function () {
    function MessageController() {
        _classCallCheck(this, MessageController);

        this.router = router;
        this.initMiddleware();
        this.initRoutes();
    }

    _createClass(MessageController, [{
        key: 'initRoutes',
        value: function initRoutes() {
            this.router.get('/', this.get);
            this.router.get('/read', this.readMessage);
            this.router.post('/', this.create);
            this.router.put('/', this.update);
            this.router.delete('/', this.delete);
        }
    }, {
        key: 'initMiddleware',
        value: function initMiddleware() {
            this.router.use(_AuthCheck2.default.request);
        }
    }, {
        key: 'get',
        value: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
                var userID, _req$query, chatID, _req$query$skip, skip, _req$query$limit, limit, dialog;

                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                userID = req.user._id;
                                _req$query = req.query, chatID = _req$query.chatID, _req$query$skip = _req$query.skip, skip = _req$query$skip === undefined ? 0 : _req$query$skip, _req$query$limit = _req$query.limit, limit = _req$query$limit === undefined ? 25 : _req$query$limit;
                                _context.next = 4;
                                return _DialogModel.DialogModel.findOne({
                                    chat: chatID,
                                    user: userID
                                }).populate({
                                    path: 'messages',
                                    select: '-__v',
                                    options: {
                                        sort: {
                                            createdAt: -1
                                        },
                                        limit: parseInt(limit),
                                        skip: parseInt(skip)
                                    }
                                }).select('messages');

                            case 4:
                                dialog = _context.sent;


                                res.status(200).json({
                                    messages: dialog.messages
                                });

                            case 6:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function get(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return get;
        }()
    }, {
        key: 'create',
        value: function () {
            var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
                var _this = this;

                var userID, _req$body$message, chat, content, author, message, dialogs;

                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                userID = req.user._id;
                                _req$body$message = req.body.message, chat = _req$body$message.chat, content = _req$body$message.content, author = _req$body$message.author;
                                _context3.prev = 2;
                                message = new _MessageModel.MessageModel({
                                    chat: chat,
                                    content: content,
                                    author: author
                                });
                                _context3.next = 6;
                                return message.save();

                            case 6:
                                _context3.next = 8;
                                return _DialogModel.DialogModel.find({
                                    chat: chat
                                });

                            case 8:
                                dialogs = _context3.sent;


                                dialogs.forEach(function () {
                                    var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(dialog) {
                                        return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                            while (1) {
                                                switch (_context2.prev = _context2.next) {
                                                    case 0:
                                                        if (!dialog.active) {
                                                            dialog.active = true;
                                                        }

                                                        if (author != dialog.user) {
                                                            dialog.unreadCount += 1;
                                                        }

                                                        dialog.messages.push(message._id);

                                                        _context2.next = 5;
                                                        return dialog.save();

                                                    case 5:
                                                    case 'end':
                                                        return _context2.stop();
                                                }
                                            }
                                        }, _callee2, _this);
                                    }));

                                    return function (_x5) {
                                        return _ref3.apply(this, arguments);
                                    };
                                }());

                                _EventEmitter2.default.emit('message_created', message);

                                res.status(200).json({
                                    message: message
                                });

                                _context3.next = 18;
                                break;

                            case 14:
                                _context3.prev = 14;
                                _context3.t0 = _context3['catch'](2);

                                console.log(_context3.t0);
                                res.status(500).json({
                                    err: _context3.t0
                                });

                            case 18:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this, [[2, 14]]);
            }));

            function create(_x3, _x4) {
                return _ref2.apply(this, arguments);
            }

            return create;
        }()
    }, {
        key: 'update',
        value: function () {
            var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
                var _req$body$message2, _id, content, chat, message;

                return regeneratorRuntime.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                _req$body$message2 = req.body.message, _id = _req$body$message2._id, content = _req$body$message2.content, chat = _req$body$message2.chat;
                                _context4.prev = 1;
                                _context4.next = 4;
                                return _MessageModel.MessageModel.findOne({
                                    _id: _id
                                });

                            case 4:
                                message = _context4.sent;


                                message.content = content;
                                message.edited = true;

                                _context4.next = 9;
                                return message.save();

                            case 9:

                                res.status(200).json({
                                    message: message
                                });

                                _context4.next = 16;
                                break;

                            case 12:
                                _context4.prev = 12;
                                _context4.t0 = _context4['catch'](1);

                                console.log(_context4.t0);
                                res.status(500).json({
                                    err: _context4.t0
                                });

                            case 16:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this, [[1, 12]]);
            }));

            function update(_x6, _x7) {
                return _ref4.apply(this, arguments);
            }

            return update;
        }()
    }, {
        key: 'delete',
        value: function () {
            var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res) {
                var userID, _req$query2, _id, chat, deleteFor;

                return regeneratorRuntime.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                userID = req.user._id;
                                _req$query2 = req.query, _id = _req$query2._id, chat = _req$query2.chat, deleteFor = _req$query2.deleteFor;
                                _context5.prev = 2;
                                _context5.next = 5;
                                return _DialogModel.DialogModel.update({ chat: chat, user: { $in: deleteFor } }, { $pull: { messages: _id } }, { multi: true });

                            case 5:

                                _EventEmitter2.default.emit('message_deleted', {
                                    deleteFor: deleteFor, message: {
                                        _id: _id, chat: chat
                                    }
                                });

                                res.status(200).json({});

                                _context5.next = 12;
                                break;

                            case 9:
                                _context5.prev = 9;
                                _context5.t0 = _context5['catch'](2);

                                res.status(500).json({
                                    err: _context5.t0
                                });

                            case 12:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, this, [[2, 9]]);
            }));

            function _delete(_x8, _x9) {
                return _ref5.apply(this, arguments);
            }

            return _delete;
        }()
    }, {
        key: 'readMessage',
        value: function () {
            var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res) {
                var userID, chatID, socket;
                return regeneratorRuntime.wrap(function _callee6$(_context6) {
                    while (1) {
                        switch (_context6.prev = _context6.next) {
                            case 0:
                                userID = req.user._id;
                                chatID = req.query.chatID;
                                socket = res.app.socket;
                                _context6.next = 5;
                                return _MessageModel.MessageModel.update({
                                    author: { $ne: userID },
                                    read: false,
                                    chat: chatID
                                }, { read: true }, { multi: true });

                            case 5:
                                _context6.next = 7;
                                return _DialogModel.DialogModel.findOneAndUpdate({ chat: chatID, user: userID }, { unreadCount: 0 });

                            case 7:

                                socket.of('/chats').to(chatID).emit('message', {
                                    type: 'read',
                                    data: {
                                        chatID: chatID, userID: userID
                                    }
                                });

                                res.status(200).json({});

                            case 9:
                            case 'end':
                                return _context6.stop();
                        }
                    }
                }, _callee6, this);
            }));

            function readMessage(_x10, _x11) {
                return _ref6.apply(this, arguments);
            }

            return readMessage;
        }()
    }]);

    return MessageController;
}();

exports.default = new MessageController();