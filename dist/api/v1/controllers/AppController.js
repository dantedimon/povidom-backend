'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _UserModel = require('../models/UserModel');

var _UserModel2 = _interopRequireDefault(_UserModel);

var _AuthCheck = require('../middleware/AuthCheck');

var _AuthCheck2 = _interopRequireDefault(_AuthCheck);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var router = _express2.default.Router();

// models


// middleware

// controller
var AuthController = function () {
    function AuthController() {
        _classCallCheck(this, AuthController);

        this.router = router;
        this.initMiddleware();
        this.initRoutes();
    }

    _createClass(AuthController, [{
        key: 'initRoutes',
        value: function initRoutes() {
            this.router.get('/preload', this.preload);
        }
    }, {
        key: 'initMiddleware',
        value: function initMiddleware() {
            this.router.use(_AuthCheck2.default.request);
        }
    }, {
        key: 'preload',
        value: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
                var userID, user;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                userID = req.user._id;
                                _context.prev = 1;
                                _context.next = 4;
                                return _UserModel2.default.findOne({
                                    _id: userID
                                }).populate({
                                    path: 'dialogs',
                                    select: '-__v',
                                    match: {
                                        active: true
                                    },
                                    populate: [{
                                        path: 'chat',
                                        select: '-__v',
                                        populate: {
                                            path: 'members',
                                            select: '-__v -password -chats -dialogs',
                                            match: {
                                                _id: {
                                                    $nin: [userID]
                                                }
                                            }
                                        }
                                    }, {
                                        path: 'messages',
                                        select: '-__v',
                                        options: {
                                            sort: {
                                                createdAt: -1
                                            },
                                            limit: 1
                                        }
                                    }]
                                }).select('-__v -password');

                            case 4:
                                user = _context.sent;


                                res.status(200).json({
                                    user: user
                                });

                                _context.next = 12;
                                break;

                            case 8:
                                _context.prev = 8;
                                _context.t0 = _context['catch'](1);

                                console.log(_context.t0);
                                res.status(500).json({
                                    err: _context.t0
                                });

                            case 12:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this, [[1, 8]]);
            }));

            function preload(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return preload;
        }()
    }]);

    return AuthController;
}();

exports.default = new AuthController();