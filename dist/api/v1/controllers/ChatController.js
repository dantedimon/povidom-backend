'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _DialogModel = require('../models/DialogModel');

var _ChatModel = require('../models/ChatModel');

var _ChatModel2 = _interopRequireDefault(_ChatModel);

var _UserModel = require('../models/UserModel');

var _UserModel2 = _interopRequireDefault(_UserModel);

var _AuthCheck = require('../middleware/AuthCheck');

var _AuthCheck2 = _interopRequireDefault(_AuthCheck);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var router = _express2.default.Router();

// models


// middleware

// controller
var ChatController = function () {
    function ChatController() {
        _classCallCheck(this, ChatController);

        this.router = router;
        this.initMiddleware();
        this.initRoutes();
    }

    _createClass(ChatController, [{
        key: 'initRoutes',
        value: function initRoutes() {
            this.router.get('/dialog', this.getDialog);
            this.router.get('/clearHistory', this.clearHistory);
            this.router.get('/deleteDialog', this.deleteDialog);
            this.router.get('/setNotifications', this.setNotifications);
            this.router.post('/createGroup', this.createGroup);
        }
    }, {
        key: 'initMiddleware',
        value: function initMiddleware() {
            this.router.use(_AuthCheck2.default.request);
        }
    }, {
        key: 'deleteDialog',
        value: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
                var userID, chatID, socket;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                userID = req.user._id;
                                chatID = req.query.chatID;
                                socket = res.app.socket;
                                _context.next = 5;
                                return _DialogModel.DialogModel.findOneAndUpdate({ chat: chatID, user: userID }, { messages: [], unreadCount: 0, active: false });

                            case 5:

                                socket.of('/private').to(userID).emit('chat', {
                                    type: 'deleteDialog',
                                    data: {
                                        chatID: chatID, userID: userID
                                    }
                                });

                                res.status(200).json({});

                            case 7:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function deleteDialog(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return deleteDialog;
        }()
    }, {
        key: 'setNotifications',
        value: function () {
            var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
                var userID, chatID, value;
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                userID = req.user._id;
                                chatID = req.query.chatID;
                                value = req.query.value;
                                _context2.next = 5;
                                return _DialogModel.DialogModel.findOneAndUpdate({ chat: chatID, user: userID }, { notifications: value });

                            case 5:

                                res.status(200).json({});

                            case 6:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this);
            }));

            function setNotifications(_x3, _x4) {
                return _ref2.apply(this, arguments);
            }

            return setNotifications;
        }()
    }, {
        key: 'clearHistory',
        value: function () {
            var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
                var userID, chatID, socket;
                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                userID = req.user._id;
                                chatID = req.query.chatID;
                                socket = res.app.socket;
                                _context3.next = 5;
                                return _DialogModel.DialogModel.findOneAndUpdate({ chat: chatID, user: userID }, { messages: [], unreadCount: 0 });

                            case 5:

                                socket.of('/private').to(userID).emit('chat', {
                                    type: 'clearHistory',
                                    data: {
                                        chatID: chatID, userID: userID
                                    }
                                });

                                res.status(200).json({});

                            case 7:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this);
            }));

            function clearHistory(_x5, _x6) {
                return _ref3.apply(this, arguments);
            }

            return clearHistory;
        }()
    }, {
        key: 'getDialog',
        value: function () {
            var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res) {
                var ownerID, userID, chatID, chat, dialog, userDialog, memberDialog, user, member, _dialog;

                return regeneratorRuntime.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                ownerID = req.user._id;
                                userID = req.query.userID;
                                chatID = req.query.chatID;

                                if (!userID) {
                                    _context4.next = 41;
                                    break;
                                }

                                _context4.next = 6;
                                return _ChatModel2.default.findOne({
                                    members: [userID, ownerID],
                                    type: 'private'
                                });

                            case 6:
                                chat = _context4.sent;

                                if (!chat) {
                                    _context4.next = 14;
                                    break;
                                }

                                _context4.next = 10;
                                return _DialogModel.DialogModel.findOne({
                                    chat: chat._id,
                                    user: ownerID
                                }).populate({
                                    path: 'chat',
                                    select: '-__v',
                                    populate: {
                                        path: 'members',
                                        select: '-__v -password -dialogs',
                                        match: {
                                            _id: {
                                                $nin: [ownerID]
                                            }
                                        }
                                    }
                                });

                            case 10:
                                dialog = _context4.sent;


                                res.json({
                                    dialog: dialog
                                });
                                _context4.next = 39;
                                break;

                            case 14:
                                chat = new _ChatModel2.default({
                                    members: [userID, ownerID],
                                    type: 'private'
                                });

                                _context4.next = 17;
                                return chat.save();

                            case 17:
                                userDialog = new _DialogModel.DialogModel({
                                    user: ownerID,
                                    chat: chat._id,
                                    companion: userID
                                });
                                memberDialog = new _DialogModel.DialogModel({
                                    user: userID,
                                    chat: chat._id,
                                    companion: ownerID
                                });
                                _context4.next = 21;
                                return userDialog.save();

                            case 21:
                                _context4.next = 23;
                                return memberDialog.save();

                            case 23:
                                _context4.next = 25;
                                return _UserModel2.default.findOne({
                                    _id: ownerID
                                });

                            case 25:
                                user = _context4.sent;
                                _context4.next = 28;
                                return _UserModel2.default.findOne({
                                    _id: userID
                                });

                            case 28:
                                member = _context4.sent;


                                user.dialogs.push(userDialog._id);
                                member.dialogs.push(memberDialog._id);

                                _context4.next = 33;
                                return user.save();

                            case 33:
                                _context4.next = 35;
                                return member.save();

                            case 35:
                                _context4.next = 37;
                                return _DialogModel.DialogModel.findOne({
                                    chat: chat._id,
                                    user: ownerID
                                }).populate({
                                    path: 'chat',
                                    select: '-__v',
                                    populate: {
                                        path: 'members',
                                        select: '-__v -password -dialogs',
                                        match: {
                                            _id: {
                                                $nin: [ownerID]
                                            }
                                        }
                                    }
                                });

                            case 37:
                                _dialog = _context4.sent;


                                res.json({
                                    dialog: _dialog
                                });

                            case 39:
                                _context4.next = 42;
                                break;

                            case 41:
                                if (chatID) {
                                    res.json({});
                                } else {
                                    res.json({});
                                }

                            case 42:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this);
            }));

            function getDialog(_x7, _x8) {
                return _ref4.apply(this, arguments);
            }

            return getDialog;
        }()
    }, {
        key: 'createGroup',
        value: function () {
            var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res) {
                var _this = this;

                var _req$body, title, members, chat;

                return regeneratorRuntime.wrap(function _callee6$(_context6) {
                    while (1) {
                        switch (_context6.prev = _context6.next) {
                            case 0:
                                _req$body = req.body, title = _req$body.title, members = _req$body.members;
                                chat = new _ChatModel2.default({
                                    title: title,
                                    members: members,
                                    type: 'group'
                                });
                                _context6.next = 4;
                                return chat.save();

                            case 4:
                                _context6.next = 6;
                                return _UserModel2.default.find({
                                    _id: { $in: members }
                                });

                            case 6:
                                members = _context6.sent;


                                members.forEach(function () {
                                    var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(member) {
                                        var dialog;
                                        return regeneratorRuntime.wrap(function _callee5$(_context5) {
                                            while (1) {
                                                switch (_context5.prev = _context5.next) {
                                                    case 0:
                                                        dialog = new _DialogModel.DialogModel({
                                                            user: member._id,
                                                            chat: chat._id,
                                                            active: true
                                                        });
                                                        _context5.next = 3;
                                                        return dialog.save();

                                                    case 3:

                                                        member.dialogs.push(dialog._id);

                                                        _context5.next = 6;
                                                        return member.save();

                                                    case 6:
                                                    case 'end':
                                                        return _context5.stop();
                                                }
                                            }
                                        }, _callee5, _this);
                                    }));

                                    return function (_x11) {
                                        return _ref6.apply(this, arguments);
                                    };
                                }());

                                res.json({});

                            case 9:
                            case 'end':
                                return _context6.stop();
                        }
                    }
                }, _callee6, this);
            }));

            function createGroup(_x9, _x10) {
                return _ref5.apply(this, arguments);
            }

            return createGroup;
        }()
    }]);

    return ChatController;
}();

exports.default = new ChatController();