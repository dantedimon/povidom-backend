'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _UserModel = require('../models/UserModel');

var _UserModel2 = _interopRequireDefault(_UserModel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var router = _express2.default.Router();

// models

// controller
var AuthController = function () {
    function AuthController() {
        _classCallCheck(this, AuthController);

        this.router = router;
        this.initRoutes();
    }

    _createClass(AuthController, [{
        key: 'initRoutes',
        value: function initRoutes() {
            this.router.post('/login', this.login);
            this.router.post('/register', this.register);
        }
    }, {
        key: 'login',
        value: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
                var _req$body$credentials, username, password, user, passMatch, access_token;

                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                // parameters
                                _req$body$credentials = req.body.credentials, username = _req$body$credentials.username, password = _req$body$credentials.password;
                                _context.prev = 1;
                                _context.next = 4;
                                return _UserModel2.default.findOne({ username: username }).select('password').exec();

                            case 4:
                                user = _context.sent;

                                if (!user) {
                                    _context.next = 19;
                                    break;
                                }

                                _context.next = 8;
                                return user.validPassword(password);

                            case 8:
                                passMatch = _context.sent;

                                if (!(!user || !passMatch)) {
                                    _context.next = 13;
                                    break;
                                }

                                res.status(401).json({
                                    message: 'Wrong credentials'
                                });
                                _context.next = 17;
                                break;

                            case 13:
                                _context.next = 15;
                                return user.generateJWT();

                            case 15:
                                access_token = _context.sent;


                                res.status(200).json({
                                    access_token: access_token
                                });

                            case 17:
                                _context.next = 20;
                                break;

                            case 19:
                                res.status(401).json({
                                    message: 'Wrong credentials'
                                });

                            case 20:
                                _context.next = 25;
                                break;

                            case 22:
                                _context.prev = 22;
                                _context.t0 = _context['catch'](1);

                                res.status(500).json({
                                    err: _context.t0
                                });

                            case 25:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this, [[1, 22]]);
            }));

            function login(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return login;
        }()
    }, {
        key: 'register',
        value: function () {
            var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
                var _req$body$credentials2, username, password, usernameExist, user, access_token;

                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                // parameters
                                _req$body$credentials2 = req.body.credentials, username = _req$body$credentials2.username, password = _req$body$credentials2.password;
                                _context2.prev = 1;
                                _context2.next = 4;
                                return _UserModel2.default.usernameExist(username);

                            case 4:
                                usernameExist = _context2.sent;

                                if (!usernameExist) {
                                    _context2.next = 9;
                                    break;
                                }

                                res.status(409).json({
                                    message: 'The user with such username already exists'
                                });
                                _context2.next = 18;
                                break;

                            case 9:
                                user = new _UserModel2.default({
                                    username: username
                                });
                                _context2.next = 12;
                                return user.hashPassword(password);

                            case 12:
                                _context2.next = 14;
                                return user.save();

                            case 14:
                                _context2.next = 16;
                                return user.generateJWT();

                            case 16:
                                access_token = _context2.sent;


                                res.status(200).json({
                                    access_token: access_token
                                });

                            case 18:
                                _context2.next = 23;
                                break;

                            case 20:
                                _context2.prev = 20;
                                _context2.t0 = _context2['catch'](1);

                                res.status(500).json({
                                    err: _context2.t0
                                });

                            case 23:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this, [[1, 20]]);
            }));

            function register(_x3, _x4) {
                return _ref2.apply(this, arguments);
            }

            return register;
        }()
    }]);

    return AuthController;
}();

exports.default = new AuthController();