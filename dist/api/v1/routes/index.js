'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _AuthController = require('../controllers/AuthController');

var _AuthController2 = _interopRequireDefault(_AuthController);

var _AppController = require('../controllers/AppController');

var _AppController2 = _interopRequireDefault(_AppController);

var _UserController = require('../controllers/UserController');

var _UserController2 = _interopRequireDefault(_UserController);

var _ChatController = require('../controllers/ChatController');

var _ChatController2 = _interopRequireDefault(_ChatController);

var _MessageController = require('../controllers/MessageController');

var _MessageController2 = _interopRequireDefault(_MessageController);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

// controllers


// routes
router.get('/', function (req, res) {
    res.send('Welcome to Povidom api v1');
});

router.use('/app', _AppController2.default.router);
router.use('/auth', _AuthController2.default.router);
router.use('/users', _UserController2.default.router);
router.use('/chats', _ChatController2.default.router);
router.use('/messages', _MessageController2.default.router);

exports.default = router;