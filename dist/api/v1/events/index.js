'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _AuthCheck = require('../middleware/AuthCheck');

var _AuthCheck2 = _interopRequireDefault(_AuthCheck);

var _EventEmitter = require('./EventEmitter');

var _EventEmitter2 = _interopRequireDefault(_EventEmitter);

var _UserModel = require('../models/UserModel');

var _UserModel2 = _interopRequireDefault(_UserModel);

var _MessageModel = require('../models/MessageModel');

var _DialogModel = require('../models/DialogModel');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Events = function () {
    function Events(socket) {
        var _this = this;

        _classCallCheck(this, Events);

        this.socket = socket;

        this.users = socket.of('/users');
        this.chats = socket.of('/chats');
        this.private = socket.of('/private');

        this.socket.use(_AuthCheck2.default.socket);
        this.users.use(_AuthCheck2.default.socket);
        this.chats.use(_AuthCheck2.default.socket);
        this.private.use(_AuthCheck2.default.socket);

        this.socket.on('connection', function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(socket) {
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _this.onConnect(socket);
                                socket.on('disconnect', _this.onDisconnect.bind(socket));

                            case 2:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, _this);
            }));

            return function (_x) {
                return _ref.apply(this, arguments);
            };
        }());

        this.chats.on('connection', function (socket) {

            socket.on('subscribe', function (ids) {
                ids.forEach(function (id) {
                    return socket.join(id);
                });
            });

            socket.on('unsubscribe', function (ids) {
                ids.forEach(function (id) {
                    return socket.leave(id);
                });
            });

            socket.on('typing', function (typing) {
                var chatID = typing.chatID;
                socket.broadcast.to(chatID).emit('typing', typing);
            });
        });

        this.private.on('connection', function (socket) {

            socket.on('subscribe', function () {
                socket.join(socket.user._id);
            });

            socket.on('unsubscribe', function () {
                socket.leave(socket.user._id);
            });
        });

        this.users.on('connection', function () {
            var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(socket) {
                var activity;
                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                activity = { online: true, _id: socket.user._id, lastActivity: Date.now() };
                                _context3.next = 3;
                                return _UserModel2.default.updateStatus(activity._id, activity);

                            case 3:

                                socket.broadcast.to(socket.user._id).emit('activity', activity);

                                socket.on('disconnect', _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                                    var activity;
                                    return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                        while (1) {
                                            switch (_context2.prev = _context2.next) {
                                                case 0:
                                                    activity = { online: false, _id: socket.user._id, lastActivity: Date.now() };
                                                    _context2.next = 3;
                                                    return _UserModel2.default.updateStatus(activity._id, activity);

                                                case 3:

                                                    socket.broadcast.to(socket.user._id).emit('activity', activity);

                                                case 4:
                                                case 'end':
                                                    return _context2.stop();
                                            }
                                        }
                                    }, _callee2, _this);
                                })));

                                socket.on('subscribe', function (ids) {
                                    ids.forEach(function (id) {
                                        return socket.join(id);
                                    });
                                });

                                socket.on('unsubscribe', function (ids) {
                                    ids.forEach(function (id) {
                                        return socket.leave(id);
                                    });
                                });

                            case 7:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, _this);
            }));

            return function (_x2) {
                return _ref2.apply(this, arguments);
            };
        }());

        // emit event on db model change
        _MessageModel.MessageEvents.on('create', function (message) {

            _this.chats.to(message.chat).emit('message', {
                type: 'new',
                data: {
                    message: message
                }
            });
        });

        _MessageModel.MessageEvents.on('update', function (message) {

            _this.chats.to(message.chat).emit('message', {
                type: 'update',
                data: {
                    message: message
                }
            });
        });

        _MessageModel.MessageEvents.on('remove', function (message) {
            console.log(message);
            _this.chats.to(message.chat).emit('message', {
                type: 'delete',
                data: {
                    message: message
                }
            });
        });

        _DialogModel.DialogEvents.on('partial:active', function (dialog) {

            _this.private.to(dialog.user).emit('chat', {
                type: 'new',
                data: {
                    dialog: dialog
                }
            });
        });

        _DialogModel.DialogEvents.on('create', function (dialog) {
            if (dialog.chat.type === 'group') {
                _this.private.to(dialog.user).emit('chat', {
                    type: 'new',
                    data: {
                        dialog: dialog
                    }
                });
            }
        });

        // MyEmitter.on('message_created', (message) => {
        //     console.log('Message created ', message);
        // });

        _EventEmitter2.default.on('message_deleted', function (data) {
            console.log('Message deleted ', data.message);

            _this.chats.to(data.message.chat).emit('message', {
                type: 'delete',
                data: data
            });
        });
    }

    _createClass(Events, [{
        key: 'onConnect',
        value: function onConnect(socket) {
            Users[socket.user._id] = socket;
        }
    }, {
        key: 'onDisconnect',
        value: function onDisconnect(reason) {
            delete Users[this.user._id];
        }
    }]);

    return Events;
}();

exports.default = Events;