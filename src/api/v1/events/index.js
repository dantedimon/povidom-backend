import AuthCheck from '../middleware/AuthCheck'

import MyEmitter from './EventEmitter'
import UserModel from '../models/UserModel'

import {MessageModel, MessageEvents} from '../models/MessageModel'
import {DialogModel, DialogEvents} from '../models/DialogModel'

class Events {
    constructor (socket) {
        this.socket = socket;

        this.users = socket.of('/users');
        this.chats = socket.of('/chats');
        this.private = socket.of('/private');

        this.socket.use(AuthCheck.socket);
        this.users.use(AuthCheck.socket);
        this.chats.use(AuthCheck.socket);
        this.private.use(AuthCheck.socket);

        this.socket.on('connection', async (socket) => {
            this.onConnect(socket);
            socket.on('disconnect', this.onDisconnect.bind(socket));
        });

        this.chats.on('connection', (socket) => {

            socket.on('subscribe', ids => {
                ids.forEach(id => socket.join(id))
            });

            socket.on('unsubscribe', ids => {
                ids.forEach(id => socket.leave(id))
            });

            socket.on('typing', (typing) => {
                let chatID = typing.chatID;
                socket.broadcast.to(chatID).emit('typing', typing);
            });
        });

        this.private.on('connection', (socket) => {

            socket.on('subscribe', () => {
                socket.join(socket.user._id)
            });

            socket.on('unsubscribe', () => {
                socket.leave(socket.user._id)
            });
        });

        this.users.on('connection', async (socket) => {

            let activity = {online: true, _id: socket.user._id, lastActivity: Date.now()};

            await UserModel.updateStatus(activity._id, activity);

            socket.broadcast.to(socket.user._id).emit('activity', activity);

            socket.on('disconnect', async () => {
                let activity = {online: false, _id: socket.user._id, lastActivity: Date.now()};

                await UserModel.updateStatus(activity._id, activity);

                socket.broadcast.to(socket.user._id).emit('activity', activity);
            });

            socket.on('subscribe', ids => {
                ids.forEach(id => socket.join(id))
            });

            socket.on('unsubscribe', ids => {
                ids.forEach(id => socket.leave(id))
            });
        });

        // emit event on db model change
        MessageEvents.on('create', (message) => {

            this.chats
                .to(message.chat)
                .emit('message', {
                    type: 'new',
                    data: {
                        message
                    }
                });
        });

        MessageEvents.on('update', (message) => {

            this.chats
                .to(message.chat)
                .emit('message', {
                    type: 'update',
                    data: {
                        message
                    }
                });
        });

        MessageEvents.on('remove', (message) => {
            console.log(message);
            this.chats
                .to(message.chat)
                .emit('message', {
                    type: 'delete',
                    data: {
                        message
                    }
                });
        });

        DialogEvents.on('partial:active', dialog => {

            this.private.to(dialog.user).emit('chat', {
                type: 'new',
                data: {
                    dialog
                }
            })
        });

        DialogEvents.on('create', dialog => {
            if (dialog.chat.type === 'group') {
                this.private.to(dialog.user).emit('chat', {
                    type: 'new',
                    data: {
                        dialog
                    }
                })
            }
        });

        // MyEmitter.on('message_created', (message) => {
        //     console.log('Message created ', message);
        // });

        MyEmitter.on('message_deleted', (data) => {
            console.log('Message deleted ', data.message);

            this.chats
                .to(data.message.chat)
                .emit('message', {
                    type: 'delete',
                    data
                });
        });
    }

    onConnect (socket) {
        Users[socket.user._id] = socket;
    }

    onDisconnect (reason) {
        delete Users[this.user._id]
    }
}

export default Events