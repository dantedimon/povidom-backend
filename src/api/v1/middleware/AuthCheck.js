import jwt from 'jsonwebtoken'
import UserModel from '../models/UserModel'

import {jwtCfg} from "../../../config"

class AuthCheck {
    static async request (req, res, next) {
        try {
            const token = req.headers.authorization.split(" ")[1];

            // console.log('token: ' + token);

            if (token) {
                let user = jwt.verify(token, jwtCfg.secret);

                user = await UserModel.findOne({
                    _id: user._id
                });

                if (user) {
                    req.user = user;
                    next();
                } else {
                    return res.status(401).json({
                        message: 'You have no permission to access this data'
                    });
                }

            } else {
                return res.status(401).json({
                    message: 'You have no permission to access this data'
                });
            }
        } catch (error) {
            if (error.name === 'TokenExpiredError'){
                return res.status(401).json({
                    message: 'Your access token has been expired. Please login again'
                });
            } else if (error.name === 'JsonWebTokenError') {
                return res.status(401).json({
                    message: 'You have no permission to access this data'
                });
            }
        }
    }

    static socket (socket, next) {
        try {
            const token = socket.handshake.query.token;

            if (token) {
                socket.user = jwt.verify(token, jwtCfg.secret);
                next();
            } else {
                next(new Error('Authentication error'))
            }
        } catch (error) {
            next(new Error('Authentication error'))
        }
    }
}

export default AuthCheck;