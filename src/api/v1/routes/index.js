import express from 'express';
const router = express.Router();

// controllers
import AuthController from '../controllers/AuthController'
import AppController from '../controllers/AppController'
import UserController from '../controllers/UserController'
import ChatController from '../controllers/ChatController'
import MessageController from '../controllers/MessageController'

// routes
router.get('/', (req, res) => {
    res.send('Welcome to Povidom api v1');
});

router.use('/app', AppController.router);
router.use('/auth', AuthController.router);
router.use('/users', UserController.router);
router.use('/chats', ChatController.router);
router.use('/messages', MessageController.router);

export default router