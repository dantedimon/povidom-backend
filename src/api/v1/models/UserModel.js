import mongoose from 'mongoose'
import {jwtCfg} from "../../../config";

import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

const Schema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    online: {
        type: Boolean,
        default: false
    },
    lastActivity: {
        type: Number,
        default: Date.now()
    },
    avatar: {
        type: String,
        default: ''
    },
    palette: {
        type: Number,
        default: Math.floor(Math.random() * (17 + 1))
    },
    dialogs: {
        type: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'dialog'
            }
        ],
        default: []
    }
});

// methods
Schema.methods.hashPassword = function (password) {
    const saltRounds = 10;

    return bcrypt.hash(password, saltRounds)
        .then(hash => {
            if (hash) {
                this.password = hash
            }
        })
};

Schema.methods.validPassword = function (password) {
    return bcrypt.compare(password, this.password)
};

Schema.methods.generateJWT = function () {
    let payload, cert, setup;

    payload = {
        _id: this._id
    };

    cert = jwtCfg.secret;

    setup = {
        expiresIn: jwtCfg.expireIn
    };

    return jwt.sign(payload, cert);
};

// statics
Schema.statics.updateStatus = function (userID, {online, lastActivity}) {
    return this.findByIdAndUpdate(userID, {
        $set: {lastActivity, online}
    })
};

Schema.statics.usernameExist = function (username) {
    return this.findOne({
        username
    })
};

Schema.statics.getSelf = function (userID) {
    return this.findOne({
        _id: userID
    })
        .toSelf()
};

// queries
Schema.query.toSelf = function () {
    return this.select('-password -chats -__v')
};

Schema.query.toContactPrev = function () {
    return this.select('-password -__v')
};

/**
 *
 * @class User
 */
const User = mongoose.model('user', Schema);

export default User;