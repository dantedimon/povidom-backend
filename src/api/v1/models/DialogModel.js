import mongoose from 'mongoose'
import MongooseTrigger from 'mongoose-trigger'

const Schema = new mongoose.Schema({
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'user'
        },
        companion: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'user'
        },
        chat: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'chat'
        },
        messages: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'message'
            }
        ],
        notifications: {
            type: Boolean,
            default: true
        },
        active: {
            type: Boolean,
            default: false
        },
        unreadCount: {
            type: Number,
            default: 0
        }
    },
    {
        timestamps: true
    });

const DialogEvents = MongooseTrigger(Schema, {
    events: {
        create: {
            populate: [
                {
                    path: 'chat',
                    select: '-__v',
                    populate: {
                        path: 'members',
                        select: '-__v -password -dialogs'
                    }
                },
                {
                    path: 'messages',
                    select: '-__v',
                }
            ]
        },
        update: true,
        remove: true
    },
    partials: [
        {
            eventName: 'active',
            triggers: 'active',
            populate: [
                {
                    path: 'chat',
                    select: '-__v',
                    populate: {
                        path: 'members',
                        select: '-__v -password -dialogs'
                    }
                },
                {
                    path: 'messages',
                    select: '-__v',
                }
            ]
        }
    ],
    debug: false
});

/**
 *
 * @Class DialogModel
 */

const DialogModel = mongoose.model('dialog', Schema);

export {DialogModel, DialogEvents}