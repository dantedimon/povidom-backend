import mongoose from 'mongoose'

const Schema = new mongoose.Schema({
        type: String,
        title: {
            type: String,
            default: ''
        },
        avatar: {
            type: String,
            default: ''
        },
        active: {
            type: Boolean,
            default: false
        },
        members: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'user'
            }
        ]
    },
    {
        timestamps: true,
    });

/**
 *
 * @Class ChatModel
 */
const ChatModel = mongoose.model('chat', Schema);

export default ChatModel;