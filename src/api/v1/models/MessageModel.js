import mongoose from 'mongoose'
import MongooseTrigger from 'mongoose-trigger'

const Schema = new mongoose.Schema({
        chat: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'chat',
            required: true
        },
        content: {
            type: String,
            required: true
        },
        author: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'user',
            required: true
        },
        read: {
            type: Boolean,
            default: false
        },
        edited: {
            type: Boolean,
            default: false
        }
    },
    {
        timestamps: true
    });

const MessageEvents = MongooseTrigger(Schema, {
    events: {
        create: true,
        update: true,
        remove: {
            select: 'chat',
        }
    },
    partials: [
        {
            eventName: 'active',
            triggers: 'active',
            populate: [
                {
                    path: 'chat',
                    select: '-__v',
                    populate: {
                        path: 'members',
                        select: '-__v -password -dialogs'
                    }
                },
                {
                    path: 'messages',
                    select: '-__v',
                }
            ]
        }
    ],
    debug: false
});

/**
 *
 * @Class MessageModel
 */

const MessageModel = mongoose.model('message', Schema);

export {MessageModel, MessageEvents}