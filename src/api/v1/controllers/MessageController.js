import express from 'express';

const router = express.Router();

import MyEmitter from '../events/EventEmitter'

// models
import {MessageModel} from '../models/MessageModel'
import {DialogModel} from '../models/DialogModel'

// middleware
import AuthCheck from '../middleware/AuthCheck'

// controller
class MessageController {
    constructor () {
        this.router = router;
        this.initMiddleware();
        this.initRoutes()
    }

    initRoutes () {
        this.router.get('/', this.get);
        this.router.get('/read', this.readMessage);
        this.router.post('/', this.create);
        this.router.put('/', this.update);
        this.router.delete('/', this.delete);
    }

    initMiddleware () {
        this.router.use(AuthCheck.request)
    }

    async get (req, res) {
        const userID = req.user._id;
        const {chatID, skip = 0, limit = 25} = req.query;

        let dialog = await DialogModel
            .findOne({
                chat: chatID,
                user: userID
            })
            .populate({
                path: 'messages',
                select: '-__v',
                options: {
                    sort: {
                        createdAt: -1
                    },
                    limit: parseInt(limit),
                    skip: parseInt(skip)
                }
            })
            .select('messages');

        res.status(200).json({
            messages: dialog.messages
        })
    }

    async create (req, res) {
        const userID = req.user._id;
        const {chat, content, author} = req.body.message;

        try {
            let message = new MessageModel({
                chat: chat,
                content: content,
                author: author
            });

            await message.save();

            let dialogs = await DialogModel
                .find({
                    chat: chat
                });

            dialogs
                .forEach(async dialog => {
                    if (!dialog.active) {
                        dialog.active = true
                    }

                    if (author != dialog.user) {
                        dialog.unreadCount += 1;
                    }

                    dialog.messages.push(message._id);

                    await dialog.save();
                });

            MyEmitter.emit('message_created', message);

            res.status(200).json({
                message
            })

        } catch (err) {
            console.log(err);
            res.status(500).json({
                err
            })
        }
    }

    async update (req, res) {
        const {_id, content, chat} = req.body.message;

        try {
            let message = await MessageModel
                .findOne({
                    _id
                });

            message.content = content;
            message.edited = true;

            await message.save();

            res.status(200).json({
                message
            })

        } catch (err) {
            console.log(err);
            res.status(500).json({
                err
            })
        }
    }

    async delete (req, res) {
        const userID = req.user._id;

        const {_id, chat, deleteFor} = req.query;

        try {
            // let message = await MessageModel
            //     .findOne({
            //         _id, chat
            //     });
            //
            // await message.remove();

            await DialogModel
                .update(
                    {chat: chat, user: {$in: deleteFor}},
                    {$pull: {messages: _id}},
                    {multi: true}
                );

            MyEmitter.emit('message_deleted', {
                deleteFor, message: {
                    _id, chat
                }
            });

            res.status(200).json({})

        } catch (err) {
            res.status(500).json({
                err
            })
        }
    }

    async readMessage (req, res) {
        const userID = req.user._id;
        let {chatID} = req.query;

        const socket = res.app.socket;

        await MessageModel.update(
            {
                author: {$ne: userID},
                read: false,
                chat: chatID
            },
            {read: true},
            {multi: true}
        );

        await DialogModel.findOneAndUpdate(
            {chat: chatID, user: userID},
            {unreadCount: 0}
        );

        socket
            .of('/chats')
            .to(chatID)
            .emit('message', {
                type: 'read',
                data: {
                    chatID, userID
                }
            });

        res.status(200).json({})
    }
}

export default new MessageController();