import express from 'express'
const router = express.Router();

// models
import UserModel from '../models/UserModel'

// middleware
import AuthCheck from '../middleware/AuthCheck'

// controller
class UserController {
    constructor () {
        this.router = router;
        this.initMiddleware();
        this.initRoutes()
    }

    initRoutes () {
        this.router.get('/self', this.getSelf);
        this.router.get('/search', this.search);
    }

    initMiddleware () {
        this.router.use(AuthCheck.request)
    }

    async getSelf (req, res) {
        const userID = req.user._id;

        try {
            let user = await UserModel.findOne({
                _id: userID
            }).toSelf();

            res.status(200).json({
                user
            })

        } catch (err) {
            console.log(err);
            res.status(500).json({
                err
            })
        }
    }

    async search (req, res) {
        const userID = req.user._id;
        const search = req.query.search;

        const regExp = new RegExp(search, 'gi');

        try {
            let users = await UserModel.find({
                $and: [
                    {username: regExp},
                    {_id: {$nin: [userID]}}
                ]

            }).toContactPrev();

            res.status(200).json({
                users
            })

        } catch (err) {
            res.status(500).json({
                err
            })
        }
    }
}

export default new UserController();