import express from 'express'
const router = express.Router();

// models
import UserModel from '../models/UserModel'

// controller
class AuthController {
    constructor () {
        this.router = router;
        this.initRoutes();
    }

    initRoutes () {
        this.router.post('/login', this.login);
        this.router.post('/register', this.register);
    }

    async login (req, res) {
        // parameters
        let { username, password } = req.body.credentials;

        try {
            const user = await UserModel
                .findOne({username})
                .select('password')
                .exec();

            if (user) {
                const passMatch = await user.validPassword(password);

                if (!user || !passMatch) {
                    res.status(401).json({
                        message: 'Wrong credentials'
                    })
                } else {
                    let access_token = await user.generateJWT();

                    res.status(200).json({
                        access_token
                    })
                }
            } else {
                res.status(401).json({
                    message: 'Wrong credentials'
                })
            }
        } catch (err) {
            res.status(500).json({
                err
            })
        }
    }

    async register (req, res) {
        // parameters
        let { username, password } = req.body.credentials;

        try {
            let usernameExist = await UserModel.usernameExist(username);

            if (usernameExist) {
                res.status(409).json({
                    message: 'The user with such username already exists'
                })
            } else {
                let user = new UserModel({
                    username
                });

                await user.hashPassword(password);

                await user.save();

                let access_token = await user.generateJWT();

                res.status(200).json({
                    access_token
                })
            }
        } catch (err) {
            res.status(500).json({
                err
            })
        }
    }
}

export default new AuthController()