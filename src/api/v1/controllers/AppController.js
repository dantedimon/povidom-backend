import express from 'express'
const router = express.Router();

// models
import UserModel from '../models/UserModel'

// middleware
import AuthCheck from '../middleware/AuthCheck'

// controller
class AuthController {
    constructor () {
        this.router = router;
        this.initMiddleware();
        this.initRoutes();
    }

    initRoutes () {
        this.router.get('/preload', this.preload);
    }

    initMiddleware () {
        this.router.use(AuthCheck.request)
    }

    async preload (req, res) {
        const userID = req.user._id;

        try {
            let user = await UserModel
                .findOne({
                    _id: userID
                })
                .populate({
                    path: 'dialogs',
                    select: '-__v',
                    match: {
                        active: true
                    },
                    populate: [
                        {
                            path: 'chat',
                            select: '-__v',
                            populate: {
                                path: 'members',
                                select: '-__v -password -chats -dialogs',
                                match: {
                                    _id: {
                                        $nin: [userID]
                                    }
                                }
                            }
                        },
                        {
                            path: 'messages',
                            select: '-__v',
                            options: {
                                sort: {
                                    createdAt: -1
                                },
                                limit: 1
                            }
                        }]
                })
                .select('-__v -password');

            res.status(200).json({
                user
            })

        } catch (err) {
            console.log(err);
            res.status(500).json({
                err
            })
        }
    }
}

export default new AuthController();