import express from 'express';
const router = express.Router();

// models
import {DialogModel} from '../models/DialogModel'
import ChatModel from '../models/ChatModel'
import UserModel from '../models/UserModel'

// middleware
import AuthCheck from '../middleware/AuthCheck'

// controller
class ChatController {
    constructor () {
        this.router = router;
        this.initMiddleware();
        this.initRoutes();
    }

    initRoutes () {
        this.router.get('/dialog', this.getDialog);
        this.router.get('/clearHistory', this.clearHistory);
        this.router.get('/deleteDialog', this.deleteDialog);
        this.router.get('/setNotifications', this.setNotifications);
        this.router.post('/createGroup', this.createGroup);
    }

    initMiddleware () {
        this.router.use(AuthCheck.request)
    }

    async deleteDialog (req, res) {
        let userID = req.user._id;
        let chatID = req.query.chatID;

        const socket = res.app.socket;

        await DialogModel.findOneAndUpdate(
            {chat: chatID, user: userID},
            {messages: [], unreadCount: 0, active: false}
        );

        socket
            .of('/private')
            .to(userID)
            .emit('chat', {
                type: 'deleteDialog',
                data: {
                    chatID, userID
                }
            });

        res.status(200).json({})
    }

    async setNotifications (req, res) {
        let userID = req.user._id;
        let chatID = req.query.chatID;
        let value = req.query.value;

        await DialogModel.findOneAndUpdate(
            {chat: chatID, user: userID},
            {notifications: value}
        );

        res.status(200).json({

        })
    }

    async clearHistory (req, res) {
        let userID = req.user._id;
        let chatID = req.query.chatID;

        const socket = res.app.socket;

        await DialogModel.findOneAndUpdate(
            {chat: chatID, user: userID},
            {messages: [], unreadCount: 0}
        );

        socket
            .of('/private')
            .to(userID)
            .emit('chat', {
                type: 'clearHistory',
                data: {
                    chatID, userID
                }
            });

        res.status(200).json({})
    }

    async getDialog (req, res) {
        let ownerID = req.user._id;

        let userID = req.query.userID;
        let chatID = req.query.chatID;

        if (userID) {
            let chat = await ChatModel.findOne({
                members: [userID, ownerID],
                type: 'private'
            });

            if (chat) {
                let dialog = await DialogModel.findOne({
                    chat: chat._id,
                    user: ownerID
                }).populate({
                    path: 'chat',
                    select: '-__v',
                    populate: {
                        path: 'members',
                        select: '-__v -password -dialogs',
                        match: {
                            _id: {
                                $nin: [ownerID]
                            }
                        }
                    }
                });

                res.json({
                    dialog
                })
            } else {
                chat = new ChatModel({
                    members: [userID, ownerID],
                    type: 'private'
                });

                await chat.save();

                let userDialog = new DialogModel({
                    user: ownerID,
                    chat: chat._id,
                    companion: userID
                });

                let memberDialog = new DialogModel({
                    user: userID,
                    chat: chat._id,
                    companion: ownerID
                });

                await userDialog.save();
                await memberDialog.save();

                let user = await UserModel.findOne({
                    _id: ownerID
                });

                let member = await UserModel.findOne({
                    _id: userID
                });

                user.dialogs.push(userDialog._id);
                member.dialogs.push(memberDialog._id);

                await user.save();
                await member.save();

                let dialog = await DialogModel
                    .findOne({
                    chat: chat._id,
                    user: ownerID
                })
                    .populate({
                    path: 'chat',
                    select: '-__v',
                    populate: {
                        path: 'members',
                        select: '-__v -password -dialogs',
                        match: {
                            _id: {
                                $nin: [ownerID]
                            }
                        }
                    }
                });

                res.json({
                    dialog
                })
            }

        } else if (chatID) {
            res.json({})
        } else {
            res.json({})
        }
    }

    async createGroup (req, res) {
        let {title, members} = req.body;

        let chat = new ChatModel({
            title,
            members,
            type: 'group'
        });

        await chat.save();

        members = await UserModel.find({
            _id: {$in: members}
        });

        members.forEach(async member => {
            let dialog = new DialogModel({
                user: member._id,
                chat: chat._id,
                active: true
            });

            await dialog.save();

            member.dialogs.push(dialog._id);

            await member.save()
        });

        res.json({})
    }
}

export default new ChatController();
