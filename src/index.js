import "babel-core/register"
import "babel-polyfill"

require('dotenv').config();

import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import socketIO from 'socket.io'
import cors from 'cors';
import http from 'http'

// routes
import apiV1 from './api/v1/routes/index';

// events
import Events from './api/v1/events'

const app = express();
const server = http.createServer(app);
const socket = socketIO(server);

// changing the mongoose promise to global promise
mongoose.Promise = global.Promise;

// database connection
mongoose.connect(process.env.db_url, { useNewUrlParser: true })
    .then(() => {
        console.clear();
        console.log("Database connection has been established")
    })
    .catch(err => {
        console.log(err)
    });

// app setup
app.use(cors());
app.use(morgan('dev'));

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use('/api/v1', apiV1);

// error handling
app.use((req, res, next) => {
    const error = new Error('Nothing to show here :)');
    error.status = 404;
    next(error)
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    })
});

// make socket global
app.socket = socket;
new Events(socket);

server.listen(process.env.PORT || 3000, () => {
    console.log('Listening on port 3000');
});

export {app}
